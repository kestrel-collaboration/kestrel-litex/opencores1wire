#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2022 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect.csr_eventmanager import EventManager, EventSourceLevel
from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

kB = 1024
mB = 1024*kB

# OneWire interface --------------------------------------------------------------------------------

class OpenCores1WireMaster(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq, with_power_drive=False):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=8, adr_width=5)
        self.wb_irq = Signal()

        # Set up IRQ handling
        self.submodules.ev = EventManager()
        self.ev.master_interrupt = EventSourceLevel(name="IRQ", description="OneWire master core interrupt")
        self.ev.finalize()

        # OneWire bus signals
        self.onewire_data_out = Signal()
        self.onewire_data_direction = Signal()
        self.onewire_data_in = Signal()
        self.onewire_power_drive = Signal()

        self.specials += Instance("onewire_master_wishbone",
            # Configuration data
            i_sys_clk_freq = clk_freq,

            # Wishbone signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = slave_bus.dat_w,
            o_slave_wb_dat_r = slave_bus.dat_r,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,
            o_slave_irq_o  = self.wb_irq,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # OneWire interface
            o_owi_o  = self.onewire_data_out,
            o_owi_oe = self.onewire_data_direction,
            i_owi_i  = self.onewire_data_in,
            o_owi_p  = self.onewire_power_drive
        )
        # Add Verilog source files
        self.add_sources(platform)

        # I/O drivers
        self.specials += SDRTristate(
            io = pads.dq,
            o  = self.onewire_data_out,
            oe = self.onewire_data_direction,
            i  = self.onewire_data_in,
        )
        if with_power_drive:
            self.comb += pads.dq.eq(self.onewire_power_drive)

        # Generate IRQ
        # Active high logic
        self.comb += self.ev.master_interrupt.trigger.eq(self.wb_irq)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "opencores1wire").data_location
        platform.add_source(os.path.join(vdir, "onewire_master.v"))
        platform.add_source(os.path.join(vdir, "third_party/onewire_master/wishbone2bus.v"))
        platform.add_source(os.path.join(vdir, "third_party/onewire_master/sockit_owm.v"))
